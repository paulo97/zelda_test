﻿using UnityEngine;

public class PlayerCollect : MonoBehaviour
{
    [SerializeField] private GameState _gameState = default;

    private void OnTriggerEnter(Collider other)
    {
        var collectable = other.GetComponent<Collectable>();
        if (collectable != null)
        {
            _gameState.IncrementPlayerMoney(collectable.Data.Value);
            collectable.Collect();
        }
    }
}
