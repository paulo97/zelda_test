﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class Spark : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleSystem = default;
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(_particleSystem.main.duration + _particleSystem.main.startLifetime.constant);

        Destroy(gameObject);
    }
}
