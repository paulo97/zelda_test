﻿using System.Collections;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    [SerializeField] private CollectableData _data = default;
    [SerializeField] private MeshRenderer _meshRenderer = default;
    [SerializeField] private GameObject _particlePrefab = default;

    public CollectableData Data => _data;

    private Coroutine _coroutine;

    private void Start()
    {
        _meshRenderer.material.color = _data.Color;

        _coroutine = StartCoroutine(RotateTransform(2f));
    }

    private void OnDestroy()
    {
        StopCoroutine(_coroutine);
    }

    public void Collect()
    {
        ServiceLocator.Get<IAudioService>().PlaySFX(SFXs.CollectMoney);
        Instantiate(_particlePrefab, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private IEnumerator RotateTransform(float duration)
    {
        var targetTransform = transform;
        var originalAngles = targetTransform.localEulerAngles;
        var targetAngles = new Vector3(originalAngles.x, originalAngles.y + 360, originalAngles.z);

        while (true)
        {
            var timeElapsed = 0f;
            targetTransform.localEulerAngles = originalAngles;
            while (timeElapsed < duration)
            {
                targetTransform.localEulerAngles = Vector3.Lerp(originalAngles, targetAngles, timeElapsed / duration);
                timeElapsed += Time.deltaTime;
                yield return null;
            }
        }
    }
}
