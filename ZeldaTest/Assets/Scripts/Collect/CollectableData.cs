﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "CollectableData", menuName = "ScriptableObjects/CollectableData")]
public class CollectableData : ScriptableObject
{
    public int Value;
    public Color Color;
}
