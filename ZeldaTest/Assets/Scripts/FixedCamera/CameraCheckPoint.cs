﻿using UnityEngine;

public class CameraCheckPoint : MonoBehaviour
{
    [SerializeField] private Transform _cameraHolder = default;

    public Transform CameraHolder => _cameraHolder;
}
