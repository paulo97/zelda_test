﻿using System.Collections;
using UnityEngine;

public class PlayerFixedCamera : MonoBehaviour
{
    [SerializeField] private Camera _camera = default;
    [SerializeField] private float _switchHolderSpeed = 4f;

    private Coroutine _coroutine;

    private void OnTriggerEnter(Collider other)
    {
        var cameraCheckPoint = other.GetComponent<CameraCheckPoint>();
        if (cameraCheckPoint != null)
        {
            _camera.transform.SetParent(cameraCheckPoint.CameraHolder, true);

            if (_coroutine != null)
                StopCoroutine(_coroutine);

            _coroutine = StartCoroutine(UpdateTransform(Vector3.zero, Quaternion.identity));
        }
    }

    private IEnumerator UpdateTransform(Vector3 targetPosition, Quaternion targetQuaternion)
    {
        var timeElapsed = 0f;
        var cameraTransform = _camera.transform;
        var startPosition = cameraTransform.localPosition;
        var startRotation = cameraTransform.localRotation;
        var duration = Vector3.Distance(startPosition, targetPosition) / _switchHolderSpeed;

        while (timeElapsed < duration)
        {
            cameraTransform.localPosition = Vector3.Lerp(startPosition, targetPosition, timeElapsed / duration);
            cameraTransform.localRotation = Quaternion.Lerp(startRotation, targetQuaternion, timeElapsed / duration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        cameraTransform.localPosition = targetPosition;
        cameraTransform.localRotation = targetQuaternion;
    }
}
