﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "GameState", menuName = "ScriptableObjects/GameState")]
public class GameState : ScriptableObject
{
    public int PlayerMoney;
    public int TimesCaught;
    public float GameDuration;
    public string AvailableAction;

    public void IncrementPlayerMoney(int value)
    {
        PlayerMoney += value;
        MoneyChanged?.Invoke();
    }

    public void ActionAvailable(string action)
    {
        AvailableAction = action;
        ActionChanged?.Invoke();
    }

    public void Reset()
    {
        PlayerMoney = 0;
        TimesCaught = 0;
        GameDuration = 0f;
        AvailableAction = string.Empty;
    }

    public event Action MoneyChanged;
    public event Action ActionChanged;
}
