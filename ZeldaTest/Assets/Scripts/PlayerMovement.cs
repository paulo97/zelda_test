﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _forwardSpeed = 3f;
    [SerializeField] private float _turnSpeed = 200f;
    [SerializeField] private float _jumpHeight = 1f;
    [SerializeField] private float _gravityValue = 10f;
    [SerializeField] private float _grabbingSpeed = 1f;
    [SerializeField] private float _crouchingSpeed = 1f;
    [SerializeField] private float _crouchingHeight = 0.75f;
    [SerializeField] private Animator _animator = default;
    [SerializeField] private Transform _rotationTransform = default;

    private CharacterController _characterController;
    private Vector3 _playerVelocity;

    private bool _isGrabbing;
    private bool _isCrouching;
    private bool _freezeMovement;
    private Transform _grabbable;
    private Vector3 _grabbablePositionDelta;
    private float _defaultHeight;
    private float _defaultRadius;
    private Vector3 _defaultCenter;
    private float _pushPower = 1.0f;

    private void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _defaultHeight = _characterController.height;
        _defaultRadius = _characterController.radius;
        _defaultCenter = _characterController.center;
        Watcher.PlayerSpotted += OnPlayerSpotted;
    }

    private void OnDestroy()
    {
        Watcher.PlayerSpotted -= OnPlayerSpotted;
    }

    private void Update()
    {
        if (_freezeMovement)
            return;

        if (_isGrabbing)
            GrabbingMovement();
        else if (_isCrouching)
            CrouchingMovement();
        else
            FreeMovement();
    }

    private void OnPlayerSpotted()
    {
        _animator.SetBool("Crouch", false);
        _animator.SetBool("OnGround", true);
        _animator.SetFloat("Forward", 0);
        _animator.SetFloat("Turn", 0);
        _freezeMovement = true;
    }

    public void SetIsGrabbing(bool value, Transform grabbable = null)
    {
        _animator.SetBool("Crouch", value);
        _isGrabbing = value;
        _grabbable = grabbable;

        if (grabbable != null)
            _grabbablePositionDelta = grabbable.position - transform.position;
    }

    public void StartCrouching(Transform crouchingSpot)
    {
        _characterController.height = _crouchingHeight;
        _characterController.radius = Mathf.Min(_defaultRadius, _crouchingHeight / 2f);
        _characterController.center = new Vector3(_characterController.center.x, _crouchingHeight / 2f, _characterController.center.z);

        _animator.SetBool("Crouch", true);
        _isCrouching = true;
    }

    public void StopCrouching()
    {
        _characterController.height = _defaultHeight;
        _characterController.radius = _defaultRadius;
        _characterController.center = _defaultCenter;

        _animator.SetBool("Crouch", false);
        _isCrouching = false;
    }

    private void FreeMovement()
    {
        // If Player Hit the Ground on Previous Move call
        var isGrounded = _characterController.isGrounded;

        // If Player is on the Ground and has negative speed, reset its speed.
        if (isGrounded && _playerVelocity.y < 0)
        {
            _animator.SetBool("OnGround", true);
            // Resetting to zero isn't strong enough to make the player hit the ground.
            _playerVelocity.y = -0.2f;
        }

        var verticalInput = Input.GetAxis("Vertical");

        // Make moving backwards slower and unable to run
        verticalInput *= verticalInput < 0 ? 0.5f : 1;

        Move(_forwardSpeed, verticalInput);

        RotateAndTurn();

        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            _animator.SetBool("OnGround", false);
            ServiceLocator.Get<IAudioService>().PlaySFX(SFXs.Jump);
            _playerVelocity.y = Mathf.Sqrt(_jumpHeight * _gravityValue);
            _characterController.Move(_playerVelocity * Time.deltaTime);
        }

        _playerVelocity.y -= _gravityValue * Time.deltaTime;
        _characterController.Move(_playerVelocity * Time.deltaTime);
    }

    private void GrabbingMovement()
    {
        var verticalInput = Input.GetAxis("Vertical");
        Move(_grabbingSpeed, verticalInput, true);

        if (_grabbable && verticalInput < 0)
            _grabbable.transform.position = transform.position + _grabbablePositionDelta;
    }

    private void CrouchingMovement()
    {
        var verticalInput = Input.GetAxis("Vertical");
        Move(_crouchingSpeed, verticalInput);

        RotateAndTurn();
    }

    private void RotateAndTurn()
    {
        var horizontalInput = Input.GetAxis("Horizontal");
        _rotationTransform.Rotate(new Vector3(0, horizontalInput * _turnSpeed * Time.deltaTime, 0));
        _animator.SetFloat("Turn", horizontalInput);
    }

    /// <summary>Move characterController and sets animator Forward.</summary>
    /// <param name="speed">Movement speed.</param>
    /// <param name="axisInput">Axis input value. Should be between -1 and 1.</param>
    /// <param name="restrainToXOrZ">Should movement be restrained to X or Z only.</param>
    private void Move(float speed, float axisInput, bool restrainToXOrZ = false)
    {
        _animator.SetFloat("AnimationDirection", axisInput < 0 ? -1 : 1);

        var movement = _rotationTransform.TransformDirection(new Vector3(0, 0, axisInput));

        if (restrainToXOrZ)
        {
            // Restrain Movement to a single world direction: x or z
            if (Mathf.Abs(movement.x) > Mathf.Abs(movement.z))
                movement = Vector3.Scale(movement, Vector3.right);
            else
                movement = Vector3.Scale(movement, Vector3.forward);
        }

        _characterController.Move(movement * Time.deltaTime * speed);
        _animator.SetFloat("Forward", Mathf.Abs(axisInput));
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (!_isGrabbing)
            return;

        var body = hit.collider.attachedRigidbody;

        if (body == null || body.isKinematic)
            return;

        // Don't push objects below us
        if (hit.moveDirection.y < -0.3)
            return;

        var movement = hit.moveDirection;
        // Restrain Movement to a single world direction: x or z
        if (Mathf.Abs(movement.x) > Mathf.Abs(movement.z))
            movement = Vector3.Scale(movement, Vector3.right);
        else
            movement = Vector3.Scale(movement, Vector3.forward);

        body.velocity = movement * _pushPower;
    }
}
