﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI    ;

public class HUD : MonoBehaviour
{
    [SerializeField] private Button _pauseButton = default;
    [SerializeField] private GameObject _pauseScreen = default;
    [SerializeField] private TextMeshProUGUI _actionText = default;
    [SerializeField] private GameState _gameState = default;

    private void Start()
    {
        _pauseScreen.SetActive(false);
        _pauseButton.onClick.AddListener(OnPause);
        _gameState.ActionChanged += OnActionChanged;
        _gameState.ActionAvailable(string.Empty);
        UpdateDisplay();
    }

    private void OnActionChanged()
    {
        UpdateDisplay();
    }

    private void OnPause()
    {
        _pauseScreen.SetActive(true);
    }

    private void UpdateDisplay()
    {
        _actionText.text = _gameState.AvailableAction;
    }
}
