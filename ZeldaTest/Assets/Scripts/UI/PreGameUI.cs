﻿using UnityEngine;
using UnityEngine.UI;

public class PreGameUI : MonoBehaviour
{
    [SerializeField] private Button _playButton = default;
    [SerializeField] private Button _exitButton = default;

    private void Start()
    {
        _playButton.onClick.AddListener(OnPlay);
        _exitButton.onClick.AddListener(OnExit);
    }

    private void OnPlay()
    {
        ServiceLocator.Get<ILevelFlow>().StartGame();
    }

    private void OnExit()
    {
        ServiceLocator.Get<ILevelFlow>().ExitGame();
    }
}
