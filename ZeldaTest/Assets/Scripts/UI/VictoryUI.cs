﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VictoryUI : MonoBehaviour
{
    [SerializeField] private Button _replayButton = default;
    [SerializeField] private Button _exitButton = default;
    [SerializeField] private TextMeshProUGUI _results = default;
    [SerializeField] private GameState _gameState = default;

    private void Start()
    {
        _replayButton.onClick.AddListener(OnReplay);
        _exitButton.onClick.AddListener(OnExit);

        _results.text = $"Times Caught: {_gameState.TimesCaught}\n" +
            $"Rupees Collected: {_gameState.PlayerMoney}/{SceneStateTracker.GetTotalMoney()}\n" +
            $"Time : {Mathf.RoundToInt(_gameState.GameDuration)}s";
    }

    private void OnReplay()
    {
        ServiceLocator.Get<ILevelFlow>().StartGame();
    }

    private void OnExit()
    {
        ServiceLocator.Get<ILevelFlow>().ExitGame();
    }
}
