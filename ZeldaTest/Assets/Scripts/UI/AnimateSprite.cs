﻿using System.Collections;
using UnityEngine;

public class AnimateSprite : MonoBehaviour
{
    [SerializeField] private int _deltaY = 30;
    private Coroutine _coroutine;

    private void Start()
    {
        _coroutine = StartCoroutine(AnimateRectTransform(2f));
    }

    private void OnDestroy()
    {
        StopCoroutine(_coroutine);
    }

    private IEnumerator AnimateRectTransform(float duration)
    {
        var rectTransform = GetComponent<RectTransform>();
        var originalPosition = rectTransform.localPosition;
        var targetPosition = new Vector3(originalPosition.x, originalPosition.y - _deltaY, originalPosition.z);
        var halfDuration = duration / 2;

        while (true)
        {
            var timeElapsed = 0f;
            rectTransform.localPosition = originalPosition;
            while (timeElapsed < halfDuration)
            {
                rectTransform.localPosition = Vector3.Lerp(originalPosition, targetPosition, timeElapsed / halfDuration);
                timeElapsed += Time.deltaTime;
                yield return null;
            }

            timeElapsed = 0f;
            rectTransform.localPosition = targetPosition;
            while (timeElapsed < halfDuration)
            {
                rectTransform.localPosition = Vector3.Lerp(targetPosition, originalPosition, timeElapsed / halfDuration);
                timeElapsed += Time.deltaTime;
                yield return null;
            }
        }
    }
}
