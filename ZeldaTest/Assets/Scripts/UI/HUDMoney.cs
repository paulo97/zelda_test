﻿using System;
using TMPro;
using UnityEngine;

public class HUDMoney : MonoBehaviour
{
    [SerializeField] private GameState _gameState = default;
    [SerializeField] private TextMeshProUGUI _text = default;

    private void Start()
    {
        _gameState.MoneyChanged += OnMoneyChanged;
        UpdateDisplay();
    }

    private void OnMoneyChanged()
    {
        UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        _text.text = _gameState.PlayerMoney.ToString();
    }
}
