﻿using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    [SerializeField] private Button _resumeButton = default;
    [SerializeField] private Button _replayButton = default;
    [SerializeField] private Button _exitButton = default;

    private float _originalTimeScale;

    private void Start()
    {
        _resumeButton.onClick.AddListener(OnResume);
        _replayButton.onClick.AddListener(OnReplay);
        _exitButton.onClick.AddListener(OnExit);
    }

    private void OnEnable()
    {
        _originalTimeScale = Time.timeScale;
        Time.timeScale = 0;
    }

    private void OnDisable()
    {
        Time.timeScale = _originalTimeScale;
    }

    private void OnResume()
    {
        gameObject.SetActive(false);
    }

    private void OnReplay()
    {
        Time.timeScale = _originalTimeScale;
        ServiceLocator.Get<ILevelFlow>().StartGame();
    }

    private void OnExit()
    {
        ServiceLocator.Get<ILevelFlow>().ExitGame();
    }
}
