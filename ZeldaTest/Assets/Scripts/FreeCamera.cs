﻿using UnityEngine;

public class FreeCamera : MonoBehaviour
{
    [SerializeField] private float _speed = 100f;
    [SerializeField] private float _maxAngleX = 35f;
    [SerializeField] private bool _reverseY = false;
    [SerializeField] private Transform _targetTransform = default;

    private Vector3 _targetEulerAngles;
    private float _originalAngleX;

    private void Start()
    {
        _targetEulerAngles = transform.eulerAngles;
        _originalAngleX = transform.eulerAngles.x;
    }

    private void Update()
    {
        MoveCamera();

        if (Input.GetButton("Reset Camera"))
        {
            transform.localRotation = Quaternion.LookRotation(_targetTransform.forward);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x + _originalAngleX, transform.eulerAngles.y, transform.eulerAngles.z);
            _targetEulerAngles = transform.eulerAngles;
        }
    }

    private void MoveCamera()
    {
        var horizontalInput = Input.GetAxis("Mouse X");
        var verticalInput = Input.GetAxis("Mouse Y");

        if (horizontalInput != 0 || verticalInput != 0)
        {
            var eulerAngles = transform.eulerAngles;

            _targetEulerAngles = new Vector3(
                Mathf.Clamp(eulerAngles.x + (_reverseY ? -1 : 1) * verticalInput, 0f, _maxAngleX),
                eulerAngles.y + horizontalInput,
                0);
        }

        var step = _speed * Time.deltaTime;
        var newEulerAngles = Vector3.MoveTowards(transform.eulerAngles, _targetEulerAngles, step);
        transform.eulerAngles = newEulerAngles;
    }
}
