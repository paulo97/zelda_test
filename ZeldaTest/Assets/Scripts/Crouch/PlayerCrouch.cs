﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCrouch : MonoBehaviour
{
    [SerializeField] private PlayerMovement _playerMovement = default;
    [SerializeField] private GameState _gameState = default;

    private CrouchSpot _crouchAreaWithinReach;
    private bool _isCrouching;

    private void OnTriggerEnter(Collider other)
    {
        var crouchArea = other.GetComponent<CrouchSpot>();
        if (crouchArea != null)
        {
            _crouchAreaWithinReach = crouchArea;
            _gameState.ActionAvailable("Crouch");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var crouchArea = other.GetComponent<CrouchSpot>();
        if (crouchArea != null && crouchArea == _crouchAreaWithinReach)
        {
            _crouchAreaWithinReach = null;
            _gameState.ActionAvailable(string.Empty);

            if (_isCrouching)
            {
                _isCrouching = false;
                _playerMovement.StopCrouching();
            }
        }
    }

    private void Update()
    {
        if (_crouchAreaWithinReach != null && Input.GetButton("Action"))
        {
            if (!_isCrouching)
            {
                _isCrouching = true;
                _playerMovement.StartCrouching(_crouchAreaWithinReach.transform);
            }
        }
    }
}
