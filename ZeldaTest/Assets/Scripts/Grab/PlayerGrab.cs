﻿using UnityEngine;

public class PlayerGrab : MonoBehaviour
{
    [SerializeField] private PlayerMovement _playerMovement = default;
    [SerializeField] private GameState _gameState = default;

    private Grabbable _grabbableWithinReach;
    private bool _isGrabbing;

    private void OnTriggerEnter(Collider other)
    {
        var grabbable = other.GetComponent<Grabbable>();
        if (grabbable != null)
        {
            _grabbableWithinReach = grabbable;
            _gameState.ActionAvailable("Grab");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var grabbable = other.GetComponent<Grabbable>();
        if (grabbable != null && grabbable == _grabbableWithinReach)
        {
            CancelGrab();
            _grabbableWithinReach = null;
            _gameState.ActionAvailable(string.Empty);
        }
    }

    private void Update()
    {
        if (_grabbableWithinReach != null && Input.GetButton("Action"))
        {
            if (!_isGrabbing)
            {
                _isGrabbing = true;
                _playerMovement.SetIsGrabbing(true, _grabbableWithinReach.transform);
                _grabbableWithinReach.Grab();
            }
        }
        else
        {
            if (_isGrabbing)
                CancelGrab();
        }
    }

    private void CancelGrab()
    {
        _isGrabbing = false;
        _playerMovement.SetIsGrabbing(false);
        _grabbableWithinReach.Release();
    }
}
