﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollower : MonoBehaviour
{
    [SerializeField] private List<Transform> _pathTransforms = default;

    [Tooltip("Duration in seconds object will remain still after reaching a path point.")]
    [SerializeField] private float _waitOnPointDuration = 1f;
    [Tooltip("Duration in seconds object will remain still before moving for the first time.")]
    [SerializeField] private float _initialWaitDuration = 0f;
    [SerializeField] private float _speed = 10f;
    [SerializeField] private Animator _animator = default;

    private List<Vector3> _pathPoints = new List<Vector3>();
    private int pointIndex = 0;

    private IEnumerator Start()
    {
        foreach (var pathTransform in _pathTransforms)
            _pathPoints.Add(pathTransform.position);

        transform.position = _pathPoints[0];

        yield return new WaitForSeconds(_initialWaitDuration);

        yield return FollowPath();
    }

    private void OnDisable()
    {
        _animator.SetBool("Run", false);
        StopAllCoroutines();
    }

    private IEnumerator FollowPath()
    {
        while(true)
        {
            pointIndex = (pointIndex + 1) % _pathPoints.Count;

            yield return WaitOnPoint(_pathPoints[pointIndex]);

            _animator.SetBool("Run", true);

            yield return GoToPoint(_pathPoints[pointIndex]);

            _animator.SetBool("Run", false);
        }
    }

    private IEnumerator WaitOnPoint(Vector3 targetPosition)
    {
        float timeElapsed = 0;
        var startValue = transform.rotation;

        var targetDirection = targetPosition - transform.position;
        var targetQuaternion = Quaternion.LookRotation(targetDirection);

        while (timeElapsed < _waitOnPointDuration)
        {
            transform.rotation = Quaternion.Lerp(startValue, targetQuaternion, timeElapsed / _waitOnPointDuration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        transform.rotation = targetQuaternion;
    }

    private IEnumerator GoToPoint(Vector3 targetPosition)
    {
        var timeElapsed = 0f;
        var startValue = transform.position;
        var duration = Vector3.Distance(startValue, targetPosition) / _speed;

        while (timeElapsed < duration)
        {
            transform.position = Vector3.Lerp(startValue, targetPosition, timeElapsed / duration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        transform.position = targetPosition;
    }
}
