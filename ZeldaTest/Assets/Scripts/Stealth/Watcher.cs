﻿using System;
using UnityEngine;

public class Watcher : MonoBehaviour
{
    [Tooltip("Interval between raycasts in seconds")]
    [SerializeField] private float _rayCastInterval = 0.2f;
    [Tooltip("Angle between side raycasts and center raycast. Changes during runtime won't work")]
    [SerializeField] private float _fieldOfViewAngle = 30f;
    [SerializeField] private float _maxRaycastDistance = 6f;
    [Tooltip("Raycasts origin")]
    [SerializeField] private Transform _eyes = default;

    public static event Action PlayerSpotted;

    private float _time;
    private Vector3 _leftFOVDirection;
    private Vector3 _rightFOVDirection;
    private PathFollower _pathFollower;
    private bool _foundTarget;

    private void Start()
    {
        _leftFOVDirection = Quaternion.Euler(0, -_fieldOfViewAngle, 0) * Vector3.forward;
        _rightFOVDirection = Quaternion.Euler(0, _fieldOfViewAngle, 0) * Vector3.forward;

        _pathFollower = GetComponent<PathFollower>();
    }

    private void FixedUpdate()
    {
        _time += Time.deltaTime;

        if (_time > _rayCastInterval)
        {
            _time = 0;

            if (!_foundTarget)
                LookForTarget();
        }
    }

    private void LookForTarget()
    {
        CheckDirection(Vector3.forward);
        CheckDirection(_leftFOVDirection);
        CheckDirection(_rightFOVDirection);
    }

    private void CheckDirection(Vector3 direction)
    {
        RaycastHit hit;
        if (Physics.Raycast(_eyes.position, _eyes.TransformDirection(direction), out hit, _maxRaycastDistance))
        {
            // Debug.DrawRay(_eyes.position, _eyes.TransformDirection(direction) * hit.distance, Color.yellow);

            if (hit.transform.name == "Player")
            {
                Debug.Log($"Player spotted!");
                _foundTarget = true;
                PlayerSpotted?.Invoke();
                PlayerSpotted = null;
                ServiceLocator.Get<IAudioService>().PlaySFX(SFXs.PlayerSpotted);

                if (_pathFollower)
                    _pathFollower.enabled = false;

                transform.LookAt(hit.transform.position);
            }
        }
        // else
        //     Debug.DrawRay(_eyes.position, _eyes.TransformDirection(direction) * _maxRaycastDistance, Color.white);
    }
}
