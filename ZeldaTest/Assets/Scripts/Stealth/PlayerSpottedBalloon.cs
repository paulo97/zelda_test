﻿using UnityEngine;

public class PlayerSpottedBalloon : MonoBehaviour
{
    [SerializeField] private GameState _gameState = default;

    private void Start()
    {
        gameObject.SetActive(false);
        Watcher.PlayerSpotted += OnPlayerSpotted;
    }

    private void OnDestroy()
    {
        Watcher.PlayerSpotted -= OnPlayerSpotted;
    }

    private void OnPlayerSpotted()
    {
        gameObject.SetActive(true);
        _gameState.TimesCaught++;
        _gameState.ActionAvailable("Next");
    }

    private void Update()
    {
        if (gameObject.activeInHierarchy && Input.GetButton("Action"))
        {
            gameObject.SetActive(false);
            ServiceLocator.Get<IAudioService>().PlaySFX(SFXs.BackToStart);
            ServiceLocator.Get<ILevelFlow>().GoToFirstLevel();
        }
    }
}
