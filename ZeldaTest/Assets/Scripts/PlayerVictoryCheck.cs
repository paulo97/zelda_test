﻿using UnityEngine;

public class PlayerVictoryCheck : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            ServiceLocator.Get<IAudioService>().PlaySFX(SFXs.FinishLevel);
            ServiceLocator.Get<ILevelFlow>().GoToNextLevel();
        }
    }
}
