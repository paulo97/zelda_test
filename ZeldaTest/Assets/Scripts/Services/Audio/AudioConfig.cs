﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "AudioConfig", menuName = "ScriptableObjects/AudioConfig")]
public class AudioConfig : ScriptableObject
{
    [SerializeField] private AudioClip _jump = default;
    [SerializeField] private AudioClip _collectMoney = default;
    [SerializeField] private AudioClip _buttonRegular = default;
    [SerializeField] private AudioClip _playerSpotted = default;
    [SerializeField] private AudioClip _backToStart = default;
    [SerializeField] private AudioClip _finishLevel = default;

    [SerializeField] private AudioClip _menu = default;
    [SerializeField] private AudioClip _medieval = default;
    [SerializeField] private AudioClip _suspense = default;

    [SerializeField] private BGMs[] _bgmForScene = default;

    private Dictionary<SFXs, AudioClip> _clipsForSFXs = default;
    private Dictionary<BGMs, AudioClip> _clipsForBGMs = default;

    public AudioClip GetClipForSFX(SFXs sfx)
    {
        if (_clipsForSFXs == default)
        {
            _clipsForSFXs = new Dictionary<SFXs, AudioClip>
            {
                {SFXs.Jump, _jump},
                {SFXs.CollectMoney, _collectMoney},
                {SFXs.ButtonRegular, _buttonRegular},
                {SFXs.PlayerSpotted, _playerSpotted},
                {SFXs.BackToStart, _backToStart},
                {SFXs.FinishLevel, _finishLevel},
            };
        }

        if (_clipsForSFXs.ContainsKey(sfx))
        {
            return _clipsForSFXs[sfx];
        }
        else
        {
            Debug.LogError($"AudioConfig doesn't contain clip for SFX: {sfx}");
            return null;
        }
    }

    public AudioClip GetClipForBGM(BGMs bgm)
    {
        if (_clipsForBGMs == default)
        {
            _clipsForBGMs = new Dictionary<BGMs, AudioClip>
            {
                {BGMs.Menu, _menu},
                {BGMs.Medieval, _medieval},
                {BGMs.Suspense, _suspense},
            };
        }

        if (_clipsForBGMs.ContainsKey(bgm))
        {
            return _clipsForBGMs[bgm];
        }
        else
        {
            Debug.LogError($"AudioConfig doesn't contain clip for BGM: {bgm}");
            return null;
        }
    }

    public BGMs GetBGMForScene(int sceneIndex)
    {
        if (sceneIndex < _bgmForScene.Length)
        {
            return _bgmForScene[sceneIndex];
        }
        else
        {
            Debug.LogError($"AudioConfig has no BGM set for sceneIndex: {sceneIndex}");
            return default(BGMs);
        }
    }
}

public enum BGMs
{
    Menu,
    Medieval,
    Suspense,
}

public enum SFXs
{
    Jump,
    CollectMoney,
    ButtonRegular,
    PlayerSpotted,
    BackToStart,
    FinishLevel
}
