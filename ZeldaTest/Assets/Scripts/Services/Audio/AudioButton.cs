﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AudioButton : MonoBehaviour
{
    [SerializeField] private Button _button = default;
    [SerializeField] private SFXs _sfx = default;

    private void Reset()
    {
        if (_button == null)
            _button = GetComponent<Button>();
    }

    private void Start()
    {
        _button.onClick.AddListener(PlaySFX);
    }

    private void PlaySFX()
    {
        ServiceLocator.Get<IAudioService>().PlaySFX(_sfx);
    }
}
