﻿using System.Collections;
using UnityEngine;

public class AudioService : MonoBehaviour, IAudioService
{
    [SerializeField] private AudioSource _sfxAudioSource = default;
    [SerializeField] private AudioSource _bgmAudioSource = default;
    [SerializeField] private AudioConfig _config = default;

    private ISceneLoader _sceneLoader;
    private Coroutine _coroutine;
    private float _originalBGMVolume;

    private void Start()
    {
        _originalBGMVolume = _bgmAudioSource.volume;
        PlayBGM(_config.GetBGMForScene(0));
    }

    public void RegisterServices(ISceneLoader sceneLoader)
    {
        _sceneLoader = sceneLoader;
        _sceneLoader.SceneEnter += OnSceneEnter;
        _sceneLoader.SceneExit += OnSceneExit;
    }

    private void OnSceneEnter(int sceneIndex)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _bgmAudioSource.volume = _originalBGMVolume;
        PlayBGM(_config.GetBGMForScene(sceneIndex));
    }

    private void OnSceneExit()
    {
        _coroutine = StartCoroutine(FadeOut(1.5f));
    }

    public void PlaySFX(SFXs sfx)
    {
        _sfxAudioSource.clip = _config.GetClipForSFX(sfx);
        _sfxAudioSource.Play();
    }

    public void PlayBGM(BGMs bgm)
    {
        _bgmAudioSource.clip = _config.GetClipForBGM(bgm);
        _bgmAudioSource.Play();
    }

    private IEnumerator FadeOut(float duration)
    {
        var startingVolume = _bgmAudioSource.volume;
        var targetVolume = 0f;

        var timeElapsed = 0f;
        while (timeElapsed < duration)
        {
            _bgmAudioSource.volume = Mathf.Lerp(startingVolume, targetVolume, timeElapsed / duration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }
}