﻿public interface IAudioService
{
    void PlaySFX(SFXs sfx);
    void PlayBGM(BGMs bgm);
}
