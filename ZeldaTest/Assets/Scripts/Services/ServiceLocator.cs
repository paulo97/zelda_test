﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ServiceLocator : MonoBehaviour
{
    private static Dictionary<string, object> services = new Dictionary<string, object>();

    public static ServiceLocator Instance { get; private set; }

    private void Awake()
    {
        if (Instance)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;

        DontDestroyOnLoad(gameObject);

        Register();
    }

    private void Register()
    {
        var sceneLoader = Register<ISceneLoader, SceneLoader>();

        var levelFlow = Register<ILevelFlow, LevelFlow>();
        levelFlow.RegisterServices(sceneLoader);

        var audioService = Register<IAudioService, AudioService>();
        audioService.RegisterServices(sceneLoader);
    }

    /// <summary>
    /// Gets the service instance of the given type.
    /// </summary>
    /// <typeparam name="I">Service interface.</typeparam>
    /// <returns>The service instance.</returns>
    public static I Get<I>()
    {
        string key = typeof(I).Name;
        if (!services.ContainsKey(key))
        {
            Debug.LogError($"{key} not registered on ServiceLocator.");
            throw new InvalidOperationException();
        }

        return (I)services[key];
    }

    /// <summary>
    /// Registers the service.
    /// </summary>
    /// <typeparam name="I">Service interface.</typeparam>
    /// <typeparam name="T">Service type.</typeparam>
    // public void Register<I, T>() where T : I, new()
    public T Register<I, T>() where T : MonoBehaviour, I
    {
        string key = typeof(I).Name;
        if (services.ContainsKey(key))
        {
            Debug.LogError($"Attempted to register service of type {key} which is already registered with the {GetType().Name}.");
            return null;
        }

        var service = GetComponent<T>();
        if (service == null)
        {
            Debug.LogError($"Unable to register service of type {key}. No component found on ServiceLocator.");
            return null;
        }

        services.Add(key, service);
        return service;
    }

    /// <summary>
    /// Unregisters the service.
    /// </summary>
    /// <typeparam name="I">Service interface.</typeparam>
    public void Unregister<I>()
    {
        string key = typeof(I).Name;
        if (!services.ContainsKey(key))
        {
            Debug.LogError($"Attempted to unregister service of type {key} which is not registered with the {GetType().Name}.");
            return;
        }

        services.Remove(key);
    }
}