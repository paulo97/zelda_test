﻿using System;

public interface ISceneLoader
{
    event Action<int> SceneEnter;
    event Action SceneExit;

    void LoadScene(int sceneIndex);
}
