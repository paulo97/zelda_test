﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour, ISceneLoader
{
    [SerializeField] private Animator _animatorPrefab = default;

    private Animator _animator;

    public event Action<int> SceneEnter;
    public event Action SceneExit;

    private void Awake()
    {
        _animator = Instantiate(_animatorPrefab, transform);
    }

    public void LoadScene(int sceneIndex)
    {
        SceneExit?.Invoke();

        StartCoroutine(LoadSceneCoroutine(sceneIndex));
    }

    private IEnumerator LoadSceneCoroutine(int sceneIndex)
    {
        _animator.SetTrigger("Start");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene(sceneIndex);

        _animator.SetTrigger("End");

        yield return new WaitForSeconds(1f);

        SceneEnter?.Invoke(sceneIndex);
    }
}
