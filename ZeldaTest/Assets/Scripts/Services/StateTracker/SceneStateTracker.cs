﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneStateTracker : MonoBehaviour
{
    private static Dictionary<string, List<TransformData>> _transformsData = new Dictionary<string, List<TransformData>>();
    private static List<Transform> _targetTransforms;
    private static string _sceneName;
    private static Dictionary<string, int> _totalMoneyOnScene = new Dictionary<string, int>();

    private void Start()
    {
        ServiceLocator.Get<ISceneLoader>().SceneExit += OnSceneExit;

        _sceneName = SceneManager.GetActiveScene().name;

        var targets = FindObjectOfType<SceneStateTrackerTargets>();
        _targetTransforms = targets.TargetTransforms;

        if (_transformsData.ContainsKey(_sceneName))
        {
            LoadSavedData(_transformsData[_sceneName]);
        }

        if (!_totalMoneyOnScene.ContainsKey(_sceneName))
            SaveTotalMoney();
    }

    private void OnDestroy()
    {
        ServiceLocator.Get<ISceneLoader>().SceneExit -= OnSceneExit;
    }

    public static void ResetState()
    {
        ServiceLocator.Get<ISceneLoader>().SceneExit -= OnSceneExit;
        _transformsData.Clear();
    }

    private static void OnSceneExit()
    {
        _transformsData[_sceneName] = GetTransformsData();
    }

    private void SaveTotalMoney()
    {
        var sum = 0;

        var collectables = FindObjectsOfType<Collectable>();
        foreach (var collectable in collectables)
            sum += collectable.Data.Value;

        _totalMoneyOnScene[_sceneName] = sum;
    }

    public static int GetTotalMoney()
    {
        return _totalMoneyOnScene.Values.Sum();
    }

    private void LoadSavedData(List<TransformData> sceneTransformsData)
    {
        for (int i = 0; i < sceneTransformsData.Count; i++)
        {
            var data = sceneTransformsData[i];
            var targetTransform = _targetTransforms[i];

            targetTransform.position = data.Position;
            targetTransform.eulerAngles = data.EulerAngles;
            targetTransform.localScale = data.Scale;
        }
    }

    private static List<TransformData> GetTransformsData()
    {
        var sceneTransformsData = new List<TransformData>();
        for (int i = 0; i < _targetTransforms.Count; i++)
        {
            var targetTransform = _targetTransforms[i];

            if (targetTransform == null)
            {
                sceneTransformsData.Add(new TransformData()
                {
                    Position = -100 * Vector3.down,
                    EulerAngles = Vector3.zero,
                    Scale = Vector3.one
                });
            }
            else
            {
                sceneTransformsData.Add(new TransformData()
                {
                    Position = targetTransform.position,
                    EulerAngles = targetTransform.eulerAngles,
                    Scale = targetTransform.localScale
                });
            }
        }
        return sceneTransformsData;
    }
}
