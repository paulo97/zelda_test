﻿using System.Collections.Generic;
using UnityEngine;

public class SceneStateTrackerTargets : MonoBehaviour
{
    [SerializeField] private List<Transform> _targetTransforms = default;

    public List<Transform> TargetTransforms => _targetTransforms;
}
