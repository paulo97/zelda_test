﻿using UnityEngine;

public class TransformData
{
    public Vector3 Position;
    public Vector3 EulerAngles;
    public Vector3 Scale;

    public override string ToString() => $"Position: {Position}, EulerAngles: {EulerAngles}, Scale {Scale}";
}
