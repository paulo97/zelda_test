﻿public interface ILevelFlow
{
    void StartGame();
    void ExitGame();
    void GoToNextLevel();
    void GoToFirstLevel();
}
