﻿using UnityEngine;

public class LevelFlow : MonoBehaviour, ILevelFlow
{
    [SerializeField] private GameState _gameState = default;
    [SerializeField] private int _firstLevelSceneIndex = 1;

    private ISceneLoader _sceneLoader;
    private int _sceneIndex;

    private void Awake()
    {
        _sceneIndex = _firstLevelSceneIndex;
    }

    public void RegisterServices(ISceneLoader sceneLoader)
    {
        _sceneLoader = sceneLoader;
    }

    public void StartGame()
    {
        _gameState.Reset();
        SceneStateTracker.ResetState();
        GoToFirstLevel();
    }

    public void ExitGame()
    {
        Application.Quit();
        Debug.Log($"Application.Quit()");
    }

    public void GoToNextLevel()
    {
        _gameState.GameDuration += Time.timeSinceLevelLoad;

        _sceneIndex++;
        _sceneLoader.LoadScene(_sceneIndex);
    }

    public void GoToFirstLevel()
    {
        _sceneIndex = _firstLevelSceneIndex;
        _sceneLoader.LoadScene(_sceneIndex);
    }
}
